#ifndef _TASK_MSP_H
#define _TASK_MSP_H

#include "stm32F1xx.h"
#include "los_base.h"
#include "los_sys.h"
#include "los_typedef.h"
#include "los_task.ph"

#define KEY0        HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_8)  //KEY0按键PC8

#define LED1_ON  		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1,GPIO_PIN_RESET)
#define LED1_OFF  	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1,GPIO_PIN_SET)
#define LED1_TOGGLE HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_1)

#define BEEP_ON  		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8,GPIO_PIN_RESET)
#define BEEP_OFF  	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8,GPIO_PIN_SET)

UINT32 create_task1(void);//创建任务1
UINT32 create_task2(void);//创建任务2

#endif
