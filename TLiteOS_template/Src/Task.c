/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : task.c
  * @brief          : task program body
  ******************************************************************************/
/* USER CODE END Header */

#include "task.h"
#include "Uart_msp.h"

/* Private variables ---------------------------------------------------------*/
UINT32 g_TskHandle;

//����1
void task1(void)
{
	int count = 1;
	while (1)
  {
		Debug_Printf("This is task1,count is %d\r\n",count);
		LOS_TaskDelay(1000);
		HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_1);
		count++;
  }
}

UINT32 create_task1()
{
    UINT32 uwRet = LOS_OK;
    TSK_INIT_PARAM_S task_init_param;

    task_init_param.usTaskPrio = 0;
    task_init_param.pcName = "task1";
    task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)task1;
    task_init_param.uwStackSize = 0x200;

    uwRet = LOS_TaskCreate(&g_TskHandle, &task_init_param);
    if(LOS_OK != uwRet)
    {
        return uwRet;
    }
    return uwRet;
}

//����2
void task2(void)
{
	int count2 = 1;
	while (1)
  {
		Debug_Printf("This is task2,count is %d\r\n",count2);
		LOS_TaskDelay(2000);
		HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_0);
		count2++;
  }
}

UINT32 create_task2()
{
    UINT32 uwRet = LOS_OK;
    TSK_INIT_PARAM_S task_init_param;

    task_init_param.usTaskPrio = 1;
    task_init_param.pcName = "task2";
    task_init_param.pfnTaskEntry = (TSK_ENTRY_FUNC)task2;
    task_init_param.uwStackSize = 0x200;

    uwRet = LOS_TaskCreate(&g_TskHandle, &task_init_param);
    if(LOS_OK != uwRet)
    {
        return uwRet;
    }
    return uwRet;
}
