/******************** (C) COPYRIGHT 2016 KMUST ********************************
* File Name          : debug.h
* Author             : 王剑平
* Version            : V1.0.1
* Date               : 2/23/2016
* 修改               : 5/23/2019 修改
* Description        : 调试模块(利用COM1作为调试输出口和本地调试口)
********************************************************************************/

#ifndef __MYDEBUG_H
#define __MYDEBUG_H

#include <stdio.h>

void Debug_Printf(const void *format, ...);

#endif  /*__MYDEBUG_H*/
